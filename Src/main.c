
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f3xx_hal.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "DEFINE.h"
#include "AD8402.h"
#include "dataProcess.h"
#include <stdlib.h>
#include <string.h>
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
AD8402 TDS_OUT;
RX_buffer rxBuffer_t;
RX_TYPE rxTable_t, rxDebug_t;
TEMP_VALUE temp100k_t, temp1k_t;


uint8_t flag_debug_done = 0;
uint8_t flag_table_done = 0;
uint8_t flag_mode;
uint8_t flag1;
int16_t arrSkip[2];
int16_t arrCompare[25];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
/* Ham nay chinh tho: chinh dien tro 100k tra ve 2 gia tri o 2 ben gia tri value: vTrai > value, vPhai < value
 *
 */
void dieuChinh_100k(int16_t value)
{
	if((TDS_OUT.value_100k) < 0)  (TDS_OUT.value_100k) = 0;
	else if((TDS_OUT.value_100k) > 255)  (TDS_OUT.value_100k) = 255;
	
	AD8402_writeData(_1k, OUT_CHANNEL, 255); // ghi gia tri max cho 1k
	for((TDS_OUT.value_100k) = (TDS_OUT.value_100k); (TDS_OUT.value_100k) > -1; (TDS_OUT.value_100k)--)
	{
		AD8402_writeData(_100k, OUT_CHANNEL, (uint8_t)(TDS_OUT.value_100k));
		for(uint8_t i = 0; i < 2; i++)
		{
			DATAPROCESS_getDebugValue();
			arrSkip[i] = rxDebug_t.value[OUT_CHANNEL];
		}
		if(rxDebug_t.value[OUT_CHANNEL] > value)    // gia tri trai
		{
			temp100k_t.dataTrai = (TDS_OUT.value_100k);				   // luu lai gia tri ben trai, luu lai du lieu ghi cho 100k ben trai value
			temp100k_t.vTrai = rxDebug_t.value[OUT_CHANNEL];  // luu gia tri ADC ben trai
		}

		else		// gia tri phai
		{	
			temp100k_t.dataPhai = (TDS_OUT.value_100k);
			temp100k_t.vPhai = rxDebug_t.value[OUT_CHANNEL];				
			AD8402_writeData(_100k, OUT_CHANNEL, (uint8_t)(temp100k_t.dataTrai)); // ghi lai gia tri ben trai
			break;
		}
	}
}

void dieuChinh_1k(int16_t value)
{
//	uint8_t *arrValue100k = pAD->arrValue_100k;  // luu byte du lieu cho 100k
//	uint8_t *arrValue1k   = pAD->arrValue_1k;	 // luu byte du lieu cho 1k
//	int16_t *value1k = &(pAD->value_1k);
//	int16_t *value100k = &(pAD->value_100k);
//	uint8_t pos = pAD->pos;

//	int16_t vPhai = pTemp100k->vPhai;
//	int16_t dataTrai = pTemp100k->dataTrai;


	int16_t temp1, temp2;
	
	for(TDS_OUT.value_1k = 255; TDS_OUT.value_1k > -1; TDS_OUT.value_1k--)
	{
		AD8402_writeData(_1k, OUT_CHANNEL, (uint8_t)(TDS_OUT.value_1k));
		for(uint8_t i = 0; i < 2; i++)
		{
			DATAPROCESS_getDebugValue();
			arrSkip[i] = rxDebug_t.value[OUT_CHANNEL];
		}
		if((rxDebug_t.value[OUT_CHANNEL] > value))
		{
			temp1 = rxDebug_t.value[OUT_CHANNEL];
			temp2 = TDS_OUT.value_1k;			
		}

		else
		{
			if((temp1 - value) < (value - rxDebug_t.value[OUT_CHANNEL]))
			{
				TDS_OUT.arrValue_100k[TDS_OUT.pos] = temp100k_t.dataTrai;
				TDS_OUT.arrValue_1k[TDS_OUT.pos] = temp2;
				arrCompare[TDS_OUT.pos] = temp1;
			}

			else
			{
				TDS_OUT.arrValue_100k[TDS_OUT.pos] = temp100k_t.dataTrai;
				TDS_OUT.arrValue_1k[TDS_OUT.pos] = TDS_OUT.value_1k;
				arrCompare[TDS_OUT.pos] = rxDebug_t.value[OUT_CHANNEL];
			}			
			break;
		}
	}
	// Neu gia tri ben phai khi dieu chinh 100k nho hon gia tri can dieu chinh tiep theo
	// thi gia tri dieu chinh lan tiep theo phai tu gia tri ben trai
	if(temp100k_t.vPhai < rxTable_t.value[TDS_OUT.pos+1])
	{
		AD8402_writeData(_100k, OUT_CHANNEL, (uint8_t)temp100k_t.dataTrai); // ghi lai gia tri ben trai
		TDS_OUT.value_100k = temp100k_t.dataTrai;
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

	//HAL_TIM_Base_Start_IT(&htim2);
	
	TDS_OUT.value_100k = 255;
	TDS_OUT.value_1k = 255;
	/* set gia tri max de gia tri ADC la max */
	AD8402_writeData(_100k, OUT_CHANNEL, TDS_OUT.value_100k);
	AD8402_writeData(_1k, OUT_CHANNEL, TDS_OUT.value_1k);
	
/* tinh toan dien tro	*/
//	HAL_Delay(10);
//	DATAPROCESS_getAdcTable();
//	int16_t arr[] = {0,0,3004,2885,2300,2065,1990,1900,1831,1754,1701,1640,1531,1145,908,733,591,462,434,430,430,425,(-28)};
//	memcpy(rxTable_t.value, arr, sizeof(arr));
//	rxTable_t.countVT = 24;
//	DATAPROCESS_skipAckDebug();
/* */
/*
100k: {0,157,41,29,23,18,15,12,11,9,7,4,2,2,1,1,1,1,1,1,0,0,}
1k: {0,245,251,179,227,214,184,228,192,246,253,176,235,170,211,176,168,167,167,166,160,0,}
*/

	#define CALIB_POIN_NUM 20
	#define CALIB_POIN_START 1
	const uint16_t resTable_100k[] =  {157,41,29,23,18,15,12,11,9,7,4,2,2,1,1,1,1,1,1,0};
	const uint16_t resTable_1k[] =  {245,251,179,227,214,184,228,192,246,253,176,235,170,211,176,168,167,167,166,160};
	const uint16_t tdsTable[] = {0,9,23,31,39,50,60,71,80,91,112,214,307,400,493,600,629,629,629,629,1390};
		
	for(uint8_t i = 0; i< CALIB_POIN_NUM; i++)
	{
		AD8402_writeData(_100k, OUT_CHANNEL, resTable_100k[i]);
		AD8402_writeData(_1k, OUT_CHANNEL, resTable_1k[i]);
		HAL_GPIO_WritePin(led_stt_GPIO_Port, led_stt_Pin, GPIO_PIN_SET);
		HAL_Delay(1500);
		HAL_GPIO_WritePin(led_stt_GPIO_Port, led_stt_Pin, GPIO_PIN_RESET);
		char sentStr[50] = "";
		sprintf(sentStr,"[CALIB_TDS,1:%d:%d]",(i+ CALIB_POIN_START),tdsTable[(i+ CALIB_POIN_START)]);
		HAL_UART_Transmit(&huart1, (uint8_t*)sentStr, strlen(sentStr), 200);
		HAL_Delay(200);
	}
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
//		for(uint8_t i = 0; i < 2; i++)
//		{
//			DATAPROCESS_getDebugValue();
//			arrSkip[i] = rxDebug_t.value[OUT_CHANNEL];
//		}
//  	for(TDS_OUT.pos = 2; TDS_OUT.pos < rxTable_t.countVT; TDS_OUT.pos++)
//		{
//			if(rxDebug_t.value[OUT_CHANNEL] > rxTable_t.value[TDS_OUT.pos])
//			{
//				dieuChinh_100k(rxTable_t.value[TDS_OUT.pos]);
//				dieuChinh_1k(rxTable_t.value[TDS_OUT.pos]);
//			}
//		}

//		while(1)
//		{
//			char sentStr[100] = "100k: {";
//			for(uint8_t i = 2; i<  rxTable_t.countVT; i++)
//			{
//				char tmp[10];
//				sprintf(tmp,"%d,",TDS_OUT.arrValue_100k[i]);
//				strcat(sentStr,tmp);
//			}
//			strcat(sentStr,"}\r\n");
//			HAL_UART_Transmit(&huart1, (uint8_t*)sentStr, strlen(sentStr), 200);
//			strcpy(sentStr,"1k: {");
//			for(uint8_t i = 2; i<  rxTable_t.countVT; i++)
//			{
//				char tmp[10];
//				sprintf(tmp,"%d,",TDS_OUT.arrValue_1k[i]);
//				strcat(sentStr,tmp);
//			}
//			strcat(sentStr,"}\r\n");
//			HAL_UART_Transmit(&huart1, (uint8_t*)sentStr, strlen(sentStr), 200);
//			HAL_GPIO_TogglePin(led_stt_GPIO_Port, led_stt_Pin);
//			HAL_Delay(2000);
//		}
//		HAL_UART_Transmit(&huart1, (uint8_t*)"chung\n", 6, 100);
//		HAL_Delay(1000);
			HAL_GPIO_TogglePin(led_stt_GPIO_Port, led_stt_Pin);
			HAL_Delay(1000);
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
